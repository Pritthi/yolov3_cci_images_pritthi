(1) all but the last 3 layers are frozen under the current settings in train.py
(2) anchor sizes are fixed acc to those used for COCO training -- look at them; see if suitable for our dataset.    ---  10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
(3) look at train_bottleneck -- to see if base layer not bad enough
(4) yolo3.cfg file contains the original configuration (for 80 classes, and the entire network)


ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library

## added this line
os.chdir('Mask_RCNN') #-- run it the first start time running notebook

MODEL_DIR = os.path.join(ROOT_DIR, "logs")

